<?php
namespace App\CoreBundle\Service\ExternalTrucking;
use GuzzleHttp\Cookie\CookieJar;

/**
 * @author nikita.kozlov
 */
class CmaCgm extends ExternalTruckingAbstract
{
    /**
     * @var \DOMXPath
     */
    private $xpath;
    /**
     * External service url
     * @var string
     */
    private $serviceUrl = 'https://www.cma-cgm.com/ebusiness/tracking/search';

    /**
     * @inheritdoc
     */
    public function getData($value, $searchType = self::SEARCH_CONTAINER)
    {
        $jar = new CookieJar();

        $response = $this->getClient()->request('GET', $this->serviceUrl, [
            'query' => [
                'SearchBy' => $this->getReference($searchType),
                'Reference' => $value
            ],
            'cookies' => $jar
        ]);

        if ($response->getStatusCode() != 200) {
            throw new \Exception("Service GET url ={$this->serviceUrl} unavailable");
        }

        $html = (string)$response->getBody();

        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        $this->xpath = new \DOMXPath($dom);

        $notFound = $this->xpath->query("//h2[text()='No Results']");

        if ($notFound->length) {
            return null;
//            throw new \Exception("No data for {$value}");
        }

        $result = new DataResult(); // result

        $result->setCurrentStatus($this->readHeaderValue("Current status", true));
        $result->setPol($this->readHeaderValue("Port of Loading", true));
        $result->setLastPort($this->readHeaderValue("Last discharge port", true));
        $result->setContainerType($this->readHeaderValue("Type", true));
        $result->setContainerSize($this->readHeaderValue("Container size", true));

        $eta = $this->readHeaderValue("ETA last port");
        if ($eta) {
            $result->setEta((new \DateTime($eta))->format('Y-m-d H:i'));
        }

        $eventsTable = $this->xpath->query("//table[@id='container-moves']");

        if (!$eventsTable->length) {
            throw new \Exception("Format error {$value}: Events table not found!");
        }

        $eventRows = $this->xpath->query('//tbody/tr', $eventsTable->item(0));

        foreach ($eventRows as $i => $eventRow) {
            if ($eventRow->nodeType != XML_ELEMENT_NODE) {
                continue;
            }

            $event = new EventResult();

            foreach ($eventRow->childNodes as $item) {
                if ($item->nodeType != XML_ELEMENT_NODE) {
                    continue;
                }

                $column = ucfirst($item->attributes->getNamedItem('data-move')->nodeValue);
                $method = "set{$column}";
                $value = $column == 'Date' ? (new \DateTime($item->nodeValue))->format('Y-m-d H:i') : trim($item->nodeValue);

                if ($column == 'Status' && !trim($value)) {
                    continue(2);
                }

                if (method_exists($event, $method)) {
                    $event->$method($value);
                }
            }

            $eventKey = $eventRow->attributes->getNamedItem('class')->nodeValue == 'date-provisional' ? 'FutureEvents' : 'Events';

            if ($eventKey == "Events") {
                $result->addEvent($event);
            } else {
                $result->addFutureEvent($event);
            }
        }

        foreach ($result->getEvents() as $event) {
            //First Load = LoadOnVesselAt
            if (!$result->getLoadOnVesselAt() && preg_match('/Loaded on board/', $event->getStatus())) {
                $result->setLoadOnVesselAt($event->getDate());
            }

            if ($event->getStatus() == 'Discharged') {
                $result->setAta($event->getDate());
            }
        }

        return $result;
    }

    /**
     * Read values from header tables
     *
     * @throws \Exception
     * @return string|null
     */
    protected function readHeaderValue($field, $throwException = false) {
        $item = $this->xpath->query('//table/tr[th[contains(text(), "'.$field.'")]]/td');

        if ($throwException && $item->length == 0) {
            throw new \Exception("Format error: Field {$field} was not found !");
        }

        return $item->length == 0 ? null : trim($item->item(0)->nodeValue);
    }

    /**
     * @param $searchType
     * @throws \Exception
     * @return string
     */
    protected function getReference($searchType) {
        switch ($searchType) {
            case self::SEARCH_CONTAINER:
                return 'Container';
            case self::SEARCH_BOL:
                return 'BL';
            case self::SEARCH_BOOKING:
                return 'Booking';
            default:
                throw new \Exception('Unknown search type');
        }
    }
}