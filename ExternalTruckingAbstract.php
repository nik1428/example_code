<?php
namespace App\CoreBundle\Service\ExternalTrucking;
use GuzzleHttp;
/**
 * Get trucking data from external sites
 *
 * @author nikita.kozlov
 */
abstract class ExternalTruckingAbstract
{
    /**
     * Search by container
     */
    const SEARCH_CONTAINER = 1;
    /**
     * Search by Bill of landing
     */
    const SEARCH_BOL = 2;
    /**
     * Search by Bill of landing
     */
    const SEARCH_BOOKING = 3;

    /**
     * @var GuzzleHttp\Client
     */
    private $client;

    /**
     *
     */
    public function __construct() {
        $this->client = new GuzzleHttp\Client([
            'headers' => ['User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'],
            'timeout'         => 0,
            'verify'  => false
        ]);
    }

    public function getClient() {
        return $this->client;
    }

    /**
     * Get single dom node value from node list at 0-index, or null when empty result
     *
     * @param \DOMNodeList $item
     * @throws \Exception
     * @return string|null
     */
    protected function getDomNodeValue(\DOMNodeList $item) {
        if ($item->length == 0) {
            return null;
        }

        return $item->item(0) instanceof \DOMAttr ? trim($item->item(0)->value) : trim($item->item(0)->nodeValue);
    }

    /**
     * Get data from external site
     *
     * @param string $value Search by this value
     * @param integer $searchType
     * @return DataResult
     */
    abstract public function getData($value, $searchType = self::SEARCH_CONTAINER);
} 